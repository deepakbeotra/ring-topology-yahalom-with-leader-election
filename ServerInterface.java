/**
 *
 * @Filename ServerInterface.java
 *
 * @Version $Id: ServerInterface.java,v 1.0 2014/05/14 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This is the remote interface for the Server
 * 
 * @author Deepak Mahajan
 * 
 */
public interface ServerInterface extends Remote {
	
	// Sets the name for the node
	public String setNodeName() throws RemoteException;
	
	// Updates the bootstrap node
	public void updateEntryNode(Node nd, String x) throws RemoteException;
	
	// Gets the bootstrap node
	public Node getEntryNode(Node nd)throws RemoteException;
	
	// Sets the unique randomn value assigned to each node for election
	public Integer setValue() throws RemoteException;
}
