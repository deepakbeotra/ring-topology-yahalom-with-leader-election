/**
 *
 * @Filename Key.java
 *
 * @Version $Id: Key.java,v 1.0 2014/05/14 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.io.Serializable;
import java.math.BigInteger;

/**
 * This class represents the key for 
 * encryption decryption
 * 
 * @author Deepak Mahajan
 * 
 */
public class Key implements Serializable {
	BigInteger mod;
	BigInteger exp;
	String nodeName;
	Key(String nodeName,BigInteger mod, BigInteger exp )
	{
		this.nodeName=nodeName;
		this.mod= mod;
		this.exp = exp;
		
	}
	

}
