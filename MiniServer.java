/**
 *
 * @Filename MiniServer.java
 *
 * @Version $Id: MiniServer.java,v 1.0 2014/05/14 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class represents the server representation of each node in the ring
 * 
 * @author Deepak Mahajan
 * 
 */
public class MiniServer extends UnicastRemoteObject implements
		MiniServerInterface, Serializable {

	/**
	 * Default constructor
	 * 
	 */
	protected MiniServer() throws RemoteException {
		super();

	}

	static Node node;
	static ServerInterface server;

	/**
	 * Main function
	 * 
	 */
	public static void main(String[] args) throws RemoteException,
			AlreadyBoundException, NotBoundException, IOException {

		MiniServerInterface client = new MiniServer();

		Registry reg1 = LocateRegistry.createRegistry(12888);

		Scanner sc = new Scanner(System.in);
		String input = null;
		boolean flag = false;
		while (true) {
			input = sc.next();
			if (input.equalsIgnoreCase("join") && flag == false) {
				System.out.println("Enter the Ip Address of server:");
				String serIp = sc.next();
				Registry registry2 = LocateRegistry.getRegistry(serIp, 12899);
				flag = true;
				reg1.rebind("client", client);
				server = (ServerInterface) registry2.lookup("bootServer");
				System.out.println("connection created in local side");
				node = new Node(server);
				System.out.println("Local Server");
				Node entryNode = server.getEntryNode(node);
				node.joinNode(entryNode);
				node.electionStart();
			} else if (input.equalsIgnoreCase("leader") && flag == true) {
				node.electionStart();
			} else if (input.equalsIgnoreCase("join") && flag == true) {
				System.out.println("Already Connected");
			} else if (input.equalsIgnoreCase("view") && flag == true) {
				System.out.print(node.nodeName + "  Left Nbr: "
						+ node.left.nodeName + "  Right Nbr: "
						+ node.right.nodeName);
				System.out.print("  Value: " + node.value);
				if (node.leader != null) {
					System.out.println("  Leader: " + node.leader.nodeName);
				} else {
					System.out.println();
				}
			} else if (input.equalsIgnoreCase("leave") && flag == true) {
				node.leave();
				System.out.println("Succesfully Leave the System");
				reg1.unbind("client");
				flag = false;

			} else if (input.equalsIgnoreCase("send") && flag == true) {
				String targetNode = sc.next();
				String msg = sc.nextLine();
				try {
					node.sendMsg(targetNode, msg);
				} catch (Exception e) {
					// System.out.println();
				}

			} else if (input.equalsIgnoreCase("election") && flag == true) {
				node.electionStart();
			} else {
				System.out.println("Enter valid Input Command....");
				System.out
						.println("Valid Input Commands: 1. join 2. send 3. leader 4. election 5. view 6. leave");
			}

		}
	}

	/**
	 * Leader receives request for communication
	 * 
	 */
	public Key callLeaderR(byte[] data) throws IOException, RemoteException {
		System.out.println("Msg Received to leader " + data);
		// checkTarget()
		return node.receiveMsgL(data);
	}

	/**
	 * Receiving token for election remotely
	 * 
	 */
	public void tokenPassR(ArrayList<Integer> token, Integer i)
			throws RemoteException, IOException {
		node.tokenReceiveL(token, i);
	}

	/**
	 * To change the left neighbour in join
	 * 
	 */
	public Node changeLftNbrR(Node nd) throws RemoteException {
		return node.changeLftNbrL(nd);
	}

	/**
	 * Change neighbour in case of leave
	 * 
	 */
	public void changeNbrR(Integer i, Node nd) throws RemoteException {
		node.changeNbrL(i, nd);
	}

	/**
	 * Sets the leader remotely
	 * 
	 */
	public void leaderElectedR(Node nd, BigInteger mod, BigInteger exp)
			throws RemoteException, IOException {
		node.setLeaderL(nd, mod, exp);
	}

	/**
	 * Leader receives public keys of all nodes
	 * 
	 */
	public void sendKeyR(String nodeName, BigInteger mod, BigInteger exp)
			throws RemoteException {
		node.storeKeysL(nodeName, mod, exp);
	}

	/**
	 * receiver receives 1st message from sender in 
	 * Yahalom including a random number
	 * 
	 */
	public Boolean firstMessageR(Integer value, byte[] encryptedMsg)
			throws IOException, RemoteException {
		return node.firstMessageReceiveL(value, encryptedMsg);
	}

	/**
	 * Leader receives message from receiver including both random
	 * numbers
	 * 
	 */
	public Boolean secondMessageR(byte[] secondEncryptedMsg)
			throws IOException, RemoteException {
		return node.secondMessageReceiveL(secondEncryptedMsg);
	}

	/**
	 * sender receives message from leader containing the key
	 * and authentication message along with randomn numbers
	 * 
	 */
	public Boolean thirdMessageR(byte[] thirdEncryptedMsgA,
			byte[] thirdEncryptedMsgB) throws IOException, RemoteException {
		return node
				.thirdMessageReceiveL(thirdEncryptedMsgA, thirdEncryptedMsgB);

	}

	/**
	 * Receiver receives the final message
	 * containing sender's message and authentication
	 * 
	 */
	public Boolean finalMessageR(byte[] finalEncryptedMsg,
			byte[] thirdEncryptedMsgB) throws IOException, RemoteException {
		return node.finalMessageReceiveL(finalEncryptedMsg, thirdEncryptedMsgB);
	}

	/**
	 * To get the request to complete the ring
	 * in case of failure
	 * 
	 */
	public Node ringCompleteR(Node nd) throws RemoteException {
		return node.ringCompleteL(nd);
	}
	

}