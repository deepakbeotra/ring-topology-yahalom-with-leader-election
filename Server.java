/**
 *
 * @Filename Server.java
 *
 * @Version $Id: Server.java,v 1.0 2014/05/14 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * This class reperesents the bootstrap server
 * 
 * @author Deepak Mahajan
 * 
 */
public class Server extends UnicastRemoteObject implements ServerInterface {
	int i = 0;
	ArrayList<Integer> values = new ArrayList<Integer>();
	Node entryNode = null;
	Integer val;

	/**
	 * Default constructor
	 * 
	 */
	public Server() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * sets the name for the node
	 * 
	 */
	public String setNodeName() throws RemoteException {
		i++;
		return "Node" + i;
	}

	/**
	 * This function sets a unique randomn value for each node on the basis of
	 * which a leader will be elected
	 * 
	 */
	public Integer setValue() throws RemoteException {
		boolean flag = false;
		Random rd = new Random();
		val = rd.nextInt(99999);
		Iterator<Integer> it = values.iterator();
		while (it.hasNext()) {
			if (val.equals(it.next())) {
				flag = true;
				break;
			}

		}
		if (flag) {
			val = setValue();
		}
		values.add(val);

		return val;
	}

	/**
	 * This function updates the entry point node or bootstrap node if it
	 * crashes
	 * 
	 */
	public void updateEntryNode(Node nd, String x) throws RemoteException {

		if (entryNode.ipAddress.equals(nd.ipAddress) && x.equals("update")) {
			System.out.println("Root Node " + entryNode.nodeName
					+ " updated at Server");
			entryNode = nd;
		} else if (entryNode.ipAddress.equals(nd.ipAddress)
				&& x.equals("change")) {
			if (entryNode.ipAddress.equals(nd.right.ipAddress)) {
				entryNode = null;
			} else {
				entryNode = nd.right;

				System.out
						.println("Root Node changed to " + entryNode.nodeName);
			}
		}
	}

	/**
	 * This function gets the bootstrap node
	 * 
	 */
	public Node getEntryNode(Node nd) throws RemoteException {
		if (entryNode == null) {
			entryNode = nd;
			System.out.println("node joined " + nd.nodeName
					+ "  and set as boot node");
			return entryNode;
		} else {
			System.out.println("node joined " + nd.nodeName);
			return entryNode;

		}
	}

	/**
	 * Main function
	 * 
	 */
	public static void main(String args[]) throws RemoteException,
			AlreadyBoundException {

		// Binding server in registry
		ServerInterface boot = new Server();

		Registry registry = LocateRegistry.createRegistry(12899);

		// Binds the registry for the object of server
		registry.rebind("bootServer", boot);
		System.out.println("server created... registry bound");

	}

}
