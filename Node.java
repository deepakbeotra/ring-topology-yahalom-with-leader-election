/**
 *
 * @Filename Node.java
 *
 * @Version $Id: Node.java,v 1.0 2014/05/14 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.StringTokenizer;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * This class reperesents each node in the ring
 * 
 * @author Deepak Mahajan
 * 
 */
public class Node implements Serializable {
	ArrayList<Integer> token;
	ArrayList<Key> keys;
	String nodeName;
	String ipAddress;
	Node left;
	Node right;
	Node leader;
	ServerInterface server;
	String LEADER_PUBLIC_KEY = "leaderPublicKey";
	String MY_PUBLIC_KEY = null;
	String MY_PRIVATE_KEY = null;
	Integer value;
	BigInteger modPub;
	BigInteger expPub;
	BigInteger modPriv;
	BigInteger expPriv;
	Integer Ra;
	Integer Rb;
	String actualMsg;
	String senderName;
	String recieverName;

	/**
	 * Parameterized constructor
	 * 
	 * @param server1
	 *            bootstrap server object
	 * 
	 */
	Node(ServerInterface server1) throws IOException {
		server = server1;
		keys = new ArrayList<Key>();
		left = this;
		right = this;
		leader = null;
		try {
			ipAddress = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e1) {
			// e1.printStackTrace();
		}
		KeyPairGenerator keyPairGenerator;
		try {

			// Generating RSA public private keys for each node

			System.out
					.println("Generating my Public and Private keys......... ");
			keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(2736);
			KeyPair keyPair = keyPairGenerator.generateKeyPair();
			PublicKey publicKey = keyPair.getPublic();
			PrivateKey privateKey = keyPair.getPrivate();
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec rsaPubKeySpec;

			rsaPubKeySpec = keyFactory.getKeySpec(publicKey,
					RSAPublicKeySpec.class);
			modPub = rsaPubKeySpec.getModulus();

			expPub = rsaPubKeySpec.getPublicExponent();

			RSAPrivateKeySpec rsaPrivKeySpec;
			rsaPrivKeySpec = keyFactory.getKeySpec(privateKey,
					RSAPrivateKeySpec.class);
			modPriv = rsaPrivKeySpec.getModulus();
			expPriv = rsaPrivKeySpec.getPrivateExponent();
			value = server.setValue();
			nodeName = server.setNodeName();
			MY_PRIVATE_KEY = nodeName + "PrivateKey";
			MY_PUBLIC_KEY = nodeName + "PublicKey";
			saveMyKeysL(MY_PUBLIC_KEY, modPub, expPub);
			saveMyKeysL(MY_PRIVATE_KEY, modPriv, expPriv);
			System.out.println("Keys generated successfully. ");
		} catch (InvalidKeySpecException e) {
			// e.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			// e1.printStackTrace();
		} catch (RemoteException e) {
			// e.printStackTrace();
		}
	}

	/**
	 * This function joins the node to the ring
	 * 
	 * @param nd
	 *            node to be joined
	 * 
	 */
	public void joinNode(Node nd) {
		if (!ipAddress.equals(nd.ipAddress)) {
			// updating my right nbr i.e. entryNode's left
			right = nd;
			// updating my left nbr i.e. other's right
			MiniServerInterface obj = circuit(nd.ipAddress);
			try {

				left = obj.changeLftNbrR(this);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			MiniServerInterface obj1 = circuit(left.ipAddress);
			try {
				obj1.changeNbrR(1, this);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		}
	}

	/**
	 * This method sets the credentials for the leader
	 * 
	 * 
	 */
	public void setLeaderL(Node nd, BigInteger mod, BigInteger exp)
			throws IOException {
		keys.clear();// empty the list of keys
		if (ipAddress.equals(nd.ipAddress)) {
			// i am also a leader
			leader = nd;
			saveMyKeysL(LEADER_PUBLIC_KEY, mod, exp);
			informOthers(nd, mod, exp);
			MiniServerInterface obj = circuit(leader.ipAddress);
			try {
				obj.sendKeyR(nodeName, modPub, expPub);
			} catch (RemoteException e) {
				System.out
						.println("Couldn't send my public key to leader as leader not found");
				leader = null;
				electionStart();
			} catch (NullPointerException ex) {
				System.out
						.println("Couldn't send my public key to leader as leader not found");
				leader = null;
				electionStart();
			}

		} else {
			leader = nd;
			saveMyKeysL(LEADER_PUBLIC_KEY, mod, exp);
			System.out.print("3rd Token received: " + nd.nodeName
					+ " is elected as a Leader ");
			informOthers(nd, mod, exp);
			MiniServerInterface obj = circuit(leader.ipAddress);
			try {
				obj.sendKeyR(nodeName, modPub, expPub);
			} catch (RemoteException e) {
				System.out
						.println("Couldn't send my public key to leader as leader not found");
				leader = null;
				electionStart();
			} catch (NullPointerException ex) {
				System.out
						.println("Couldn't send my public key to leader as leader not found");
				leader = null;
				electionStart();
			}
		}
	}

	/**
	 * This method is used to inform all the nodes about the leader
	 * 
	 * 
	 */
	public void informOthers(Node nd, BigInteger mod, BigInteger exp)
			throws IOException {
		if (!(right.ipAddress.equals(nd.ipAddress))) {
			System.out.println("and Token passed to " + right.nodeName);
			MiniServerInterface obj = circuit(right.ipAddress);
			try {
				obj.leaderElectedR(nd, mod, exp);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}

		} else {
			System.out.println();
		}
	}

	/**
	 * This method is used to receive a token during election
	 * 
	 * @param token
	 *            All the tokens from nodes
	 * @param i
	 *            represents type of token
	 * 
	 */
	public void tokenReceiveL(ArrayList<Integer> token, Integer i)
			throws IOException {
		// Leader Election token being received.
		// Node puts its own value or token in it in forwards
		if (i.equals(1)) {
			// If all the tokens have been accumulated
			if (token.get(0).equals(value)) {
				// 1st Ring Completed
				System.out.println("1st round completed with Token " + token
						+ " and Computation for the Leader is started");
				Iterator<Integer> it = token.iterator();
				Integer max = it.next();
				while (it.hasNext()) {
					Integer temp = it.next();
					if (max.compareTo(temp) < 0) {
						max = temp;
					}
				}

				// To calculate leader
				if (max.equals(value)) {
					System.out.println("I found myself as a leader with value "
							+ max);
					System.out
							.print("3rd Token created and passed in the ring to inform that I am elected as a Leader ");
					// inform others that I am leader --------->>
					setLeaderL(this, modPub, expPub);
				} else {
					System.out.println("Leader is selectd with value " + max);
					System.out
							.println("2nd token created and passed in the ring to"
									+ " inform node with value "
									+ max
									+ " about its leadership");
					token.clear();
					token.add(value); // to stop token.... 0th position
										// creater's info
					token.add(max); // leader information
					MiniServerInterface obj = circuit(right.ipAddress);
					try {
						obj.tokenPassR(token, 2);
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
				}
			}

			// Node has not yet put its value in the list.
			// It puts its value and forwards the list
			else {
				System.out.println("1st Token Received for election ");
				token.add(value);
				System.out.println("Value " + value + " added in the Token "
						+ token + " and passed in the ring ");
				MiniServerInterface obj = circuit(right.ipAddress);
				try {
					obj.tokenPassR(token, 1);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					System.out
							.println("right nbr dosnt exist........ so lets first fix it");
					right = ringCompleteL(this);
					System.out.println("Ring Completed");
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					System.out
							.println("right nbr dosnt exist........ so lets first fix it");
					right = ringCompleteL(this);
					System.out.println("Ring Completed");
				}
			}
		}

		// If token received is that which tells the node about the leader
		else if (i.equals(2)) {
			System.out.println("2nd token received");
			// token again reaches to start position, that is leader could found
			if (token.get(0).equals(value)) {
				electionStart();
			} else if (token.get(1).equals(value)) {
				System.out
						.println("I am informed that I am a elected as a Leader in 2nd token");
				System.out
						.print("3rd Token created and passed in the ring to inform that I am elected as a Leader ");

				setLeaderL(this, modPub, expPub);
				// inform others------------>
			} else {
				MiniServerInterface obj = circuit(right.ipAddress);
				try {
					obj.tokenPassR(token, 2);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}
			}
		}
	}

	/**
	 * This method is used to complete the ring in case of failure
	 * 
	 * 
	 * @param nd
	 *            node that identifies a crash
	 * 
	 */
	public Node ringCompleteL(Node nd) {
		MiniServerInterface obj = circuit(left.ipAddress);
		try {
			return obj.ringCompleteR(nd);
		} catch (NullPointerException e) {
			System.out.println("null pointer exception");
			System.out.println("other end found");
			left = nd;
			return this;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			System.out
					.println("Left neighbor changed due to brokage of ring...");
			left = nd;
			System.out
					.println("--------------------------------------------------------------------");
			return this;
		}
	}

	/**
	 * This method is used to store the public key of all the nodes by the
	 * leader
	 * 
	 */
	public void storeKeysL(String nodeName, BigInteger mod, BigInteger exp) {

		Key k = new Key(nodeName, mod, exp);
		keys.add(k);
		System.out.println("Public key of " + nodeName + " received");
	}

	/**
	 * This method is used to change a node's neighbours
	 * 
	 */
	public void changeNbrL(Integer i, Node nd) {
		if (i.equals(1)) {
			right = nd;
		}

		else if (i.equals(2)) {
			left = nd;
		}

		try {
			server.updateEntryNode(this, "update");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	/**
	 * This method is used to change only the left neighbour in case of leave
	 * 
	 */
	public Node changeLftNbrL(Node nd) {
		Node temp = left;
		left = nd;
		try {
			server.updateEntryNode(this, "update");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return temp;
	}

	/**
	 * This method is used to lookup to a node to connect
	 * 
	 */
	public MiniServerInterface circuit(String ip1) {
		Registry registryCircuit;
		try {
			registryCircuit = LocateRegistry.getRegistry(ip1, 12888);

			return (MiniServerInterface) registryCircuit.lookup("client");

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			return null;
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	/**
	 * This method is used by a node to leave the ring
	 * 
	 */
	public void leave() {
		// updating my left nbr i.e. other's right
		MiniServerInterface obj = circuit(left.ipAddress);
		try {
			obj.changeNbrR(1, right);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// updating my right nbr i.e. other's left
		MiniServerInterface obj1 = circuit(right.ipAddress);
		try {
			obj1.changeNbrR(2, left);
			server.updateEntryNode(this, "change");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		left = this;
		right = this;
		leader = null;
	}

	/**
	 * This method is used to start the election algorithm
	 * 
	 */
	public void electionStart() throws IOException {
		System.out.println();
		System.out.println("Election Starts");
		System.out.println();
		token = new ArrayList<Integer>();
		token.add(value);
		System.out.println("1st Token " + token
				+ " created and passed in the ring ");
		MiniServerInterface obj = circuit(right.ipAddress);
		try {
			obj.tokenPassR(token, 1);
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			System.out
					.println("right nbr dosnt exist........ so lets first fix it");
			right = ringCompleteL(this);
			System.out.println("Ring Completed");
			System.out
					.println("--------------------------------------------------------------------");
		}
	}

	/**
	 * This method is used to save the keys for encryption and decryption in a
	 * file
	 * 
	 */
	public void saveMyKeysL(String fileName, BigInteger mod, BigInteger exp)
			throws IOException {

		FileOutputStream fos = null;
		ObjectOutputStream oos = null;

		try {

			fos = new FileOutputStream(fileName);
			// fos.write(new String().getBytes());
			oos = new ObjectOutputStream(new BufferedOutputStream(fos));

			oos.writeObject(mod);
			oos.writeObject(exp);
		} catch (Exception e) {
			// e.printStackTrace();
		} finally {
			if (oos != null) {
				oos.close();

				if (fos != null) {
					fos.close();
				}
			}
		}
	}

	/**
	 * This method is used to encrypt the data to be sent
	 * 
	 */
	private byte[] encryptData(String PublicK, String data) throws IOException {
		System.out.println("\n----------ENCRYPTION STARTED------------");

		System.out.println("Data Before Encryption :" + data);
		byte[] dataToEncrypt = data.getBytes("UTF-8");
		byte[] encryptedData = null;
		try {
			PublicKey pubKey = readPublicKeyFromFile(PublicK);
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			// cipher.update(dataToEncrypt);
			encryptedData = cipher.doFinal(dataToEncrypt);
			System.out.println("Encryted Data: " + encryptedData);

		} catch (Exception e) {
			// e.printStackTrace();
		}

		System.out.println("----------ENCRYPTION COMPLETED----------\n");
		return encryptedData;
	}

	/**
	 * This method is used to read the public key
	 * to encrypt data
	 * 
	 */
	public PublicKey readPublicKeyFromFile(String fileName) throws IOException {
		FileInputStream fileInputStream = null;
		ObjectInputStream objectOutputStream = null;
		try {
			
			// Reading file to get public key
			fileInputStream = new FileInputStream(new File(fileName));
			objectOutputStream = new ObjectInputStream(fileInputStream);
			
			// Get the mod and exponent for public key
			BigInteger mod = (BigInteger) objectOutputStream.readObject();
			BigInteger exp = (BigInteger) objectOutputStream.readObject();
			
			// Generate public key spectrum using mod and exponent
			RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(mod, exp);
			KeyFactory keyFact = KeyFactory.getInstance("RSA");
			
			// To get the public key out of the spectrum
			PublicKey publicKey = keyFact.generatePublic(rsaPublicKeySpec);

			return publicKey;

		} catch (Exception e) {
			// e.printStackTrace();
		} finally {
			if (objectOutputStream != null) {
				objectOutputStream.close();
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			}
		}
		return null;
	}

	/**
	 * This method is used to send a message to another node
	 * 
	 */
	public void sendMsg(String targetNode, String msg) throws IOException {
		System.out
				.println("--------------------------------------------------------------------");
		if (leader == null) {
			System.out.println();
			System.out
					.println("=>> No Leader!! Starting the Election and Resend the message again.... <<=");
			electionStart();
		} else if (targetNode.equals(nodeName)) {
			System.out
					.println("=>> You can't send message to yourself... Send message to some other node.... <<= ");
		} else {
			System.out.println("Leader Available....");
			Registry registryCircuit;
			MiniServerInterface obj = null;
			byte[] encryptedData = null;
			try {

				registryCircuit = LocateRegistry.getRegistry(leader.ipAddress,
						12888);
				obj = (MiniServerInterface) registryCircuit.lookup("client");
				String requestForKeyMsg = nodeName + "*##*" + targetNode;
				System.out.println();
				System.out
						.println("Packet is being prepared to send to leader to request for public key of "
								+ targetNode);
				encryptedData = encryptData(LEADER_PUBLIC_KEY, requestForKeyMsg); // encryted
																					// node
																					// name

			} catch (RemoteException e) {
				// System.out.println("Exception in sendMsg... Remote Exception");
				// TODO Auto-generated catch block
				leader = null;
				// System.out.println("Leader no more exists!!! Starting the election again.... "
				// );
				sendMsg(targetNode, msg);
			} catch (NotBoundException e) {
				leader = null;
				// System.out.println("Leader no more exists!!! Starting the election again.... "
				// );
				sendMsg(targetNode, msg);
			}

			Key targetNodeKey = obj.callLeaderR(encryptedData);
			if (targetNodeKey.nodeName.equals("Yes")) {
				System.out.println("=>> No Such Node Found.... Try Again <<= ");
			} else if (targetNodeKey.nodeName.equals("No")) {
				System.out.println("=>> You are not allowed to request <<=");

			} else if (targetNodeKey.nodeName.equals(targetNode)) {
				System.out.println("Public key of " + targetNodeKey.nodeName
						+ " is received from " + leader.nodeName);
				Random rd = new Random();
				Ra = rd.nextInt(99999);
				String firstMsg = nodeName + "*##*" + Ra;

				MiniServerInterface obj2 = circuit(right.ipAddress);
				try {
					System.out
							.println("New Random number Ra " + Ra
									+ " is generated and"
									+ " added into the packet to send to "
									+ targetNode);
					System.out
							.println("\n----------ENCRYPTION STARTED------------");
					System.out.println("Data Before Encryption :" + firstMsg);
					byte[] encryptedMsg = onSpotEncrytion(targetNodeKey.mod,
							targetNodeKey.exp, firstMsg);
					System.out.println("Encryted Data: " + encryptedMsg);
					System.out
							.println("----------ENCRYPTION COMPLETED----------\n");
					// msgSendFlag=false;
					actualMsg = msg;
					recieverName = targetNode;
					System.out
							.println("Packet prepared and forward in the Ring");
					Boolean check = obj2.firstMessageR(value, encryptedMsg);
					if (check == true) {
						System.out.println("=>> Message Reached to "
								+ targetNode + " successfully <<=");
						System.out
								.println("--------------------------------------------------------------------");
					} else {
						System.out
								.println("=>> Problem occured... Try again <<=");
						System.out
								.println("--------------------------------------------------------------------");
					}
				} catch (RemoteException e) {
					System.out
							.println("Right Nbr could Not found------ Need to complete the ring first");
					System.out
							.println("Right Nbr dosnt exist........ so lets first fix it");
					right = ringCompleteL(this);
					System.out
							.println("=>> Ring Completed... Try to send the message again.... <<=");
				} catch (NullPointerException ex) {
					System.out
							.println("Rright Nbr could Not found------ Need to complete the ring first");
					System.out
							.println("Right Nbr dosnt exist........ so lets first fix it");
					System.out
							.println("--------------------------------------------------------------------");
					right = ringCompleteL(this);
					System.out
							.println("=>> Ring Completed... Try to send the message again.... <<=");
					System.out
							.println("--------------------------------------------------------------------");
				}
			}
		}
	}

	
	/**
	 * This method is used for the sender node to
	 * send a random number to receiver for Yahalom.
	 * 
	 */
	public Boolean firstMessageReceiveL(Integer value1, byte[] encryptedMsg)
			throws IOException {
		
		//  If receiver crashes in between
		if ((this.value.equals(value1))) {
			System.out
					.println("=>> Message vanish's... Target Node no more exists!!!!!!!!!! <<=");
			return false;
		} else {
			byte[] descryptedData = null;
			PrivateKey privateKey = readPrivateKeyFromFile(MY_PRIVATE_KEY);
			Cipher cipher;
			try {
				System.out.println();
				System.out.println("Encryted Message Received " + encryptedMsg);
				cipher = Cipher.getInstance("RSA");
				cipher.init(Cipher.DECRYPT_MODE, privateKey);
				descryptedData = cipher.doFinal(encryptedMsg);
				System.out
						.println("\n----------DECRYPTION STARTED------------");
				String decryptredString = new String(descryptedData);
				System.out.println("Decrypted Data: " + decryptredString);
				System.out
						.println("----------DECRYPTION COMPLETED----------\n");
				StringTokenizer parts = new StringTokenizer(decryptredString,
						"*##*");

				senderName = parts.nextElement().toString();
				Ra = Integer.parseInt(parts.nextElement().toString());
				// msgReceiveFlag=false;
				Random rd = new Random();
				Rb = rd.nextInt(99999);
				// System.out.println("Message decryted sussesfully");
				// System.out.println();
				// System.out.println("Details-> Sender:"+senderName+" Ra:"+Ra);

				String secondMessage = nodeName + "*##*" + senderName + "*##*"
						+ Ra + "*##*" + Rb;
				byte[] secondEncryptedMsg = encryptData(LEADER_PUBLIC_KEY,
						secondMessage);
				MiniServerInterface obj2 = circuit(leader.ipAddress);
				try {
					System.out.println("Encryted Message " + secondEncryptedMsg
							+ " by adding new random number Rb " + Rb
							+ " forwarded to leader " + leader.nodeName);
					System.out.println();
					System.out.println("Waiting for msg from " + senderName
							+ "....");
					return obj2.secondMessageR(secondEncryptedMsg);
				} catch (NullPointerException exx) {
					System.out
							.println("=>> Leader not available..... Starting leader election <<=");
					electionStart();
					System.out
							.println("--------------------------------------------------------------------");

				}
			} catch (NoSuchAlgorithmException e) {
				//e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				//e.printStackTrace();
			} catch (InvalidKeyException e) {
				//e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				//e.printStackTrace();

			} catch (BadPaddingException e) {
				System.out
						.println("Couldn't Open it, Message Forward in the ring");
				MiniServerInterface obj2 = circuit(right.ipAddress);
				try {
					return obj2.firstMessageR(value1, encryptedMsg);
				} catch (NullPointerException exx) {
					// System.out.println("---------------------------- right Node could Not found------ Need to complete the ring first");
					System.out
							.println("right nbr dosnt exist........ so lets first fix it");
					right = ringCompleteL(this);
					System.out.println("Ring Completed");
					System.out
							.println("--------------------------------------------------------------------");
				}
			}
		}
		return false;
	}

	/**
	 * In this method, the leader gives the sender
	 * a message for the receiver for authentication
	 * and both the random numbers and a new key to encrypt its message
	 * 
	 */
	public Boolean thirdMessageReceiveL(byte[] thirdEncryptedA,
			byte[] thirdEncryptedB) throws IOException {
		try {

			byte[] descryptedData = null;
			PrivateKey privateKey = readPrivateKeyFromFile(MY_PRIVATE_KEY);
			Cipher cipher;
			System.out.println("Encryted Message Received " + thirdEncryptedA);
			cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			descryptedData = cipher.doFinal(thirdEncryptedA);
			String decryptredString = new String(descryptedData);
			System.out.println("\n----------DECRYPTION STARTED------------");
			System.out.println("Decrypted Data: " + decryptredString);
			System.out.println("----------DECRYPTION COMPLETED----------\n");
			StringTokenizer parts = new StringTokenizer(decryptredString,
					"*##*");

			String rName = parts.nextElement().toString();
			Integer tempRb = Integer.parseInt(parts.nextElement().toString());
			Integer tempRa = Integer.parseInt(parts.nextElement().toString());
			BigInteger tempPubMod = new BigInteger(parts.nextElement()
					.toString());
			BigInteger tempPubExp = new BigInteger(parts.nextElement()
					.toString());
			System.out.println("New Public Key received to encrypt the data ");
			// after permission and receiving key from trusted party, forward
			// the msg to reciever

			// to authenticate the receiver
			if (Ra.equals(tempRa)) {
				System.out.println(rName
						+ " is authenticated as received random number "
						+ tempRa + " is same as sent random number " + Ra);
				// encryt the final msg with new generated key
				String finalMsg = tempRb + "*##*" + actualMsg;

				System.out
						.println("\n----------ENCRYPTION STARTED------------");
				System.out.println("Data Before Encryption :" + finalMsg);
				byte[] finalEncryptedMsg = onSpotEncrytion(tempPubMod,
						tempPubExp, finalMsg);
				System.out.println("Encryted Data: " + finalEncryptedMsg);
				System.out
						.println("----------ENCRYPTION COMPLETED----------\n");
				MiniServerInterface obj2 = circuit(right.ipAddress);

				System.out
						.println("Message passed in the ring and Public Key destroyed....");

				Ra = null;
				senderName = null;
				Rb = null;
				recieverName = null;
				actualMsg = null;
				return obj2.finalMessageR(finalEncryptedMsg, thirdEncryptedB);
			}

		} catch (NoSuchAlgorithmException e) {
			//e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			//e.printStackTrace();
		} catch (InvalidKeyException e) {
			//e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			//e.printStackTrace();

		} catch (BadPaddingException e) {
			System.out.println("Couldn't Open it, Message Forward in the ring");
			MiniServerInterface obj2 = circuit(right.ipAddress);
			try {
				return obj2.thirdMessageR(thirdEncryptedA, thirdEncryptedB);
			} catch (NullPointerException exx) {
				System.out
						.println("Right Nbr could Not found------ Need to complete the ring first");
				System.out
						.println("Right Nbr dosnt exist........ so lets first fix it");
				right = ringCompleteL(this);
				System.out.println("Ring Completed");
				System.out
						.println("--------------------------------------------------------------------");
			}
		}
		return false;
	}

	/**
	 * This method is used for the receiver node to
	 * receive the message and authentication from the sender
	 * encrypted by the new key gained from the leader
	 * 
	 */
	public Boolean finalMessageReceiveL(byte[] finalEncryptedMsg,
			byte[] thirdEncryptedMsgB) throws IOException {
		byte[] thirdDecryptedData = null;
		PrivateKey privateKey = readPrivateKeyFromFile(MY_PRIVATE_KEY);
		Cipher cipher;
		BigInteger tempPrivMod = null;
		BigInteger tempPrivExp = null;

		try {
			System.out.println();
			System.out.println("Encryted Message Received "
					+ thirdEncryptedMsgB);

			cipher = Cipher.getInstance("RSA");

			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			thirdDecryptedData = cipher.doFinal(thirdEncryptedMsgB);
			System.out.println("\n----------DECRYPTION STARTED------------");
			String thirdDecryptedString = new String(thirdDecryptedData);
			System.out.println("Decrypted Data: " + thirdDecryptedString);
			System.out.println("----------DECRYPTION COMPLETED----------\n");
			System.out
					.println("Received new private key to open final msg....");

			// System.out.println("Decryted data: "+thirdDecryptedString);
			StringTokenizer parts = new StringTokenizer(thirdDecryptedString,
					"*##*");
			String tempSender = parts.nextElement().toString();
			tempPrivMod = new BigInteger(parts.nextElement().toString());
			tempPrivExp = new BigInteger(parts.nextElement().toString());

			System.out.println(nodeName
					+ " checks that packet came from the same sender "
					+ tempSender + " who requested earlier");
			System.out
					.println(nodeName
							+ " will use the new private key to open the another encrypted message.. ");
		} catch (NoSuchAlgorithmException e1) {
			// e1.printStackTrace();
		} catch (NoSuchPaddingException e1) {
			// e1.printStackTrace();
		} catch (InvalidKeyException e) {
			// e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// e.printStackTrace();
		} catch (BadPaddingException e) {
			System.out.println("Can't Open it, Message Forward in the ring");
			MiniServerInterface obj2 = circuit(right.ipAddress);
			try {
				return obj2
						.finalMessageR(finalEncryptedMsg, thirdEncryptedMsgB);
			} catch (RemoteException exx) {

			}
		}

		try {
			RSAPrivateKeySpec rsaPrivateKeySpec = new RSAPrivateKeySpec(
					tempPrivMod, tempPrivExp);
			KeyFactory fact = KeyFactory.getInstance("RSA");
			PrivateKey tempPrivateKey = fact.generatePrivate(rsaPrivateKeySpec);
			Cipher cipher1 = Cipher.getInstance("RSA");
			cipher1.init(Cipher.DECRYPT_MODE, tempPrivateKey);
			byte[] finalDecryptedData = cipher1.doFinal(finalEncryptedMsg);
			System.out.println("\n----------DECRYPTION STARTED------------");
			String finalDecryptedString = new String(finalDecryptedData);
			System.out.println("Decrypted Final Data: " + finalDecryptedString);
			System.out.println("----------DECRYPTION COMPLETED----------\n");
			StringTokenizer parts1 = new StringTokenizer(finalDecryptedString,
					"*##*");
			Integer tempRb = Integer.parseInt(parts1.nextElement().toString());
			if (tempRb.equals(Rb)) {
				System.out.println("Random Number Rb " + tempRb
						+ " is same as that was sent earlier " + Rb
						+ ".... This Authenticates the " + senderName);
				String receivedMsg = parts1.nextElement().toString();
				System.out.println();
				// System.out.println(nodeName+" will believe that "+senderName+" sent him a msg  :=>  "+receivedMsg);
				System.out.println("Message received from " + senderName
						+ " :=> " + receivedMsg);
				System.out.println();
				System.out.println("Private key destroyed...");
				System.out
						.println("--------------------------------------------------------------------");
			} else {
				System.out
						.println("Random Number "
								+ tempRb
								+ " is not same as that was sent earlier....Doesnt Authenticate the "
								+ senderName);
			}
			Ra = null;
			senderName = null;
			Rb = null;
			recieverName = null;
			actualMsg = null;
		} catch (NoSuchAlgorithmException e) {

		} catch (NoSuchPaddingException e) {

		} catch (InvalidKeyException e) {

		} catch (IllegalBlockSizeException e) {

		} catch (BadPaddingException e) {

		} catch (InvalidKeySpecException e) {

		}
		return true;
	}

	/**
	 * In this method the leader receives a message from
	 * receiver with the random number from the sender, a randomn number generated by 
	 * it and node names. The message is encrypted by the leader's 
	 * public key
	 * 
	 */
	public Boolean secondMessageReceiveL(byte[] secondEncryptedMsg)
			throws IOException {
		System.out.println();
		System.out
				.println("--------------------------------------------------------------------");
		System.out.println();
		System.out.println("Leader: Second Message received "
				+ secondEncryptedMsg);
		String decryptedMsg = decryptData(MY_PRIVATE_KEY, secondEncryptedMsg);
		StringTokenizer parts = new StringTokenizer(decryptedMsg, "*##*");

		String tempReceiverName = parts.nextElement().toString();
		String tempSenderName = parts.nextElement().toString();
		Integer senderRa = Integer.parseInt(parts.nextElement().toString());
		Integer receiverRb = Integer.parseInt(parts.nextElement().toString());
		System.out.println();
		KeyPairGenerator keyPairGenerator;
		try {
			keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(512);
			KeyPair keyPair = keyPairGenerator.generateKeyPair();
			PublicKey tempPublicKey = keyPair.getPublic();
			PrivateKey tempPrivateKey = keyPair.getPrivate();
			System.out.println("Key Pair generated ......");
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec rsaPubKeySpec;

			rsaPubKeySpec = keyFactory.getKeySpec(tempPublicKey,
					RSAPublicKeySpec.class);
			BigInteger tempPubMod = rsaPubKeySpec.getModulus();
			BigInteger tempPubExp = rsaPubKeySpec.getPublicExponent();

			RSAPrivateKeySpec rsaPrivKeySpec = keyFactory.getKeySpec(
					tempPrivateKey, RSAPrivateKeySpec.class);
			BigInteger tempPrivMod = rsaPrivKeySpec.getModulus();
			BigInteger tempPrivExp = rsaPrivKeySpec.getPrivateExponent();

			String thirdMsgS = tempReceiverName + "*##*" + receiverRb + "*##*"
					+ senderRa + "*##*" + tempPubMod + "*##*" + tempPubExp;
			// System.out.println(thirdMsgS);
			System.out.println(thirdMsgS.getBytes().length);

			String thirdMsgR = tempSenderName + "*##*" + tempPrivMod + "*##*"
					+ tempPrivExp;
			// System.out.println(thirdMsgR);
			System.out.println(thirdMsgR.getBytes().length);

			Iterator<Key> kS = keys.iterator();
			BigInteger modS = null;
			BigInteger expS = null;
			// to find sender's key
			while (kS.hasNext()) {
				Key temp = kS.next();
				if (tempSenderName.equals(temp.nodeName)) {
					modS = temp.mod;
					expS = temp.exp;
					break;
				}

			}
			// to find receiver's key
			Iterator<Key> kR = keys.iterator();
			BigInteger modR = null;
			BigInteger expR = null;
			while (kR.hasNext()) {
				Key temp = kR.next();
				if (tempReceiverName.equals(temp.nodeName)) {
					modR = temp.mod;
					expR = temp.exp;
					break;
				}
			}
			System.out.println("\n----------ENCRYPTION STARTED------------");
			System.out.println("Leader:   Data Before Encryption for "
					+ tempSenderName + " (with Newly Genereated Public Key): "
					+ thirdMsgS);

			byte[] thirdEncryptedMsgS = onSpotEncrytion(modS, expS, thirdMsgS);
			System.out
					.println("Leader:   Encryted Data: " + thirdEncryptedMsgS);
			System.out.println("----------ENCRYPTION COMPLETED----------\n");

			System.out.println("\n----------ENCRYPTION STARTED------------");
			System.out.println("Leader:   Data Before Encryption for "
					+ tempReceiverName
					+ " (with Newly Genereated Private Key): " + thirdMsgR);
			byte[] thirdEncryptedMsgR = onSpotEncrytion(modR, expR, thirdMsgR);
			System.out
					.println("Leader:   Encryted Data: " + thirdEncryptedMsgR);
			System.out.println("----------ENCRYPTION COMPLETED----------\n");
			MiniServerInterface obj2 = circuit(right.ipAddress);

			System.out.println("Encrypted Message passed in the ring for "
					+ tempSenderName);
			System.out
					.println("Newly generated keys destroyed succesfully.......");
			System.out
					.println("--------------------------------------------------------------------");
			return obj2.thirdMessageR(thirdEncryptedMsgS, thirdEncryptedMsgR);

		} catch (NoSuchAlgorithmException e) {

		}

		catch (NullPointerException exx) {
			System.out
					.println("right nbr dosnt exist........ so lets first fix it");
			right = ringCompleteL(this);
			System.out.println("Ring Completed");
			System.out
					.println("--------------------------------------------------------------------");
			System.out.println();

		} catch (InvalidKeySpecException e) {

		}
		return false;

	}

	/**
	 * The leader receives a request from the
	 * sender to send a message to another node in the ring
	 * The leader authenticates it and sends receiver's public key
	 * 
	 */
	public Key receiveMsgL(byte[] data) throws IOException {
		System.out.println("Checking Requesting Node to Authenticate ....");
		String decryptedData = decryptData(MY_PRIVATE_KEY, data);
		StringTokenizer parts = new StringTokenizer(decryptedData, "*##*");
		// public StringTokenizer(String str,String delim)
		String requestNode = parts.nextElement().toString();
		String targetNodeName = parts.nextElement().toString();
		Iterator<Key> it = keys.iterator();
		boolean flag = false; // to check weather who is requesting is
								// authenticated or not
		// boolean checkRequest=false;

		// to check weather who is requesting is authenticated or not
		while (it.hasNext()) {
			Key temp1 = it.next();
			if (temp1.nodeName.equals(requestNode)) {
				flag = true;
				break;
			}
		}
		if (flag) {
			Iterator<Key> it1 = keys.iterator();
			while (it1.hasNext()) {
				Key temp = it1.next();
				if (temp.nodeName.equals(targetNodeName)) {
					// checkRequest=true;
					return temp;
				}
			}
		} else {
			Key temp = new Key(new String("No"), new BigInteger("1"),
					new BigInteger("1"));
			return temp;
		}
		// if entered flag... but didnt return
		System.out.println("No Such Node Found");
		Key temp = new Key(new String("Yes"), new BigInteger("1"),
				new BigInteger("1"));
		return temp;

	}

	/**
	 * This method is used to decrypt the data being sent
	 * 
	 */
	private String decryptData(String PrivateK, byte[] data) throws IOException {

		byte[] decryptedData = null;

		try {
			
			// Reading the private key
			PrivateKey privateKey = readPrivateKeyFromFile(PrivateK);
			Cipher cipher = Cipher.getInstance("RSA");
			
			//Initialize cipher
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			
			// Decrypt the data using private key
			decryptedData = cipher.doFinal(data);
			
			// print decrypted data
			System.out.println("\n----------DECRYPTION STARTED------------");
			String decryptedString = new String(decryptedData);
			System.out.println("Decrypted Data: " + decryptedString);
			System.out.println("----------DECRYPTION COMPLETED----------\n");
			return decryptedString;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * This method is used to read the private key
	 * 
	 */
	public PrivateKey readPrivateKeyFromFile(String fileName)
			throws IOException {
		FileInputStream fileInputStream = null;
		ObjectInputStream objectInputStream = null;
		try {

			// Reading mod and exponent from file
			fileInputStream = new FileInputStream(new File(fileName));
			objectInputStream = new ObjectInputStream(fileInputStream);

			// Mod value for the private key
			BigInteger mod = (BigInteger) objectInputStream.readObject();
			
			// Exponent value for the private key
			BigInteger exp = (BigInteger) objectInputStream.readObject();

			// Get Private Key
			RSAPrivateKeySpec rsaPrivateKeySpec = new RSAPrivateKeySpec(mod,
					exp);
			KeyFactory keyFact = KeyFactory.getInstance("RSA");
			PrivateKey privateKey = keyFact.generatePrivate(rsaPrivateKeySpec);
			return privateKey;

		} catch (Exception e) {

		} finally {
			if (objectInputStream != null) {
				objectInputStream.close();
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			}
		}
		return null;
	}

	/**
	 * For encrypting without storing any keys
	 * 
	 */
	public byte[] onSpotEncrytion(BigInteger mod, BigInteger exp, String data)
			throws IOException {
		RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(mod, exp);
		KeyFactory fact;
		PublicKey pubKey = null;
		byte[] encryptedData = null;
		byte[] dataToEncrypt = data.getBytes("UTF-8");
		try {
			fact = KeyFactory.getInstance("RSA");

			pubKey = fact.generatePublic(rsaPublicKeySpec);
		} catch (InvalidKeySpecException e) {

		} catch (NoSuchAlgorithmException e) {
		}
		try {
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			// cipher.update(dataToEncrypt);
			encryptedData = cipher.doFinal(dataToEncrypt);
		} catch (Exception e) {

		}
		return encryptedData;
	}

}
