/**
 *
 * @Filename MiniServerInterface.java
 *
 * @Version $Id: MiniServerInterface.java,v 1.0 2014/05/14 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */
import java.io.IOException;
import java.math.BigInteger;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * This interface is the remote interface
 * for the nodes in the ring
 * 
 * @author Deepak Mahajan
 * 
 */
public interface MiniServerInterface extends Remote {
	
	/**
	 * To change the left neighbour in join
	 * 
	 */
	public Node changeLftNbrR(Node nd) throws RemoteException; 
	
	/**
	 * Receiving token for election remotely
	 * 
	 */
	public void tokenPassR(ArrayList<Integer> token,Integer i) throws RemoteException,IOException;
	
	/**
	 * Sets the leader remotely
	 * 
	 */
	public void leaderElectedR(Node nd,BigInteger mod, BigInteger exp) throws RemoteException,IOException;
	
	/**
	 * Change neighbour in case of leave
	 * 
	 */
	public void changeNbrR(Integer i,Node nd) throws RemoteException;
	
	/**
	 * Leader receives request for communication
	 * 
	 */
	public Key callLeaderR( byte[] data)throws IOException,RemoteException;
	
	/**
	 * Leader receives public keys of all nodes
	 * 
	 */
	public void sendKeyR(String publicKey,BigInteger mod, BigInteger exp)throws RemoteException;
	
	/**
	 * receiver receives 1st message from sender in 
	 * Yahalom including a random number
	 * 
	 */
	public Boolean firstMessageR(Integer value,byte[]encryptedMsg)throws IOException,RemoteException;
	
	/**
	 * Leader receives message from receiver including both random
	 * numbers
	 * 
	 */
	public Boolean secondMessageR(byte[]secondEncryptedMsg)throws IOException,RemoteException;
	
	/**
	 * sender receives message from leader containing the key
	 * and authentication message along with randomn numbers
	 * 
	 */
	public Boolean thirdMessageR(byte[] thirdEncryptedMsg,byte[] thirdEncryptedMsgB)throws IOException,RemoteException;
	
	/**
	 * Receiver receives the final message
	 * containing sender's message and authentication
	 * 
	 */
	public Boolean finalMessageR(byte[]finalEncryptedMsg,byte[]thirdEncryptedMsgB)throws IOException,RemoteException;
	
	/**
	 * To get the request to complete the ring
	 * in case of failure
	 * 
	 */
	public Node ringCompleteR(Node nd) throws RemoteException;

}
